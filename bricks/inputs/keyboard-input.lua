
return function(gate, argv)
	if argv[1]=="\\:" then argv[1] = ";" end
	
	local keycode = keyboard_keyToCode[argv[1]] or keyboard_keyToCode["invalid"]
	local status = keyboard_strToBool[argv[2]]
	
	local code = keycode+(status and 128 or 0)
	
	local function queueBit(bit)
		table.insert(gate.queueBits, 1, bit)
	end
	
	queueBit(true)
	for bitidx = 1, 8 do
		local val = bit.band(code, 0x80)~=0
		queueBit(val)
		
		code = bit.lshift(code, 1)
	end
	queueBit(false)
	
	gate:queue(0)
end
