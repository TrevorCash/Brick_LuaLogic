
-- 2020-03-30
-- Requirement change: The gate's output will now depend only on the event, and not carry the input at all

return function(gate, argv)
	gate.ports[2]:setstate(toboolean(argv[1]))
end
