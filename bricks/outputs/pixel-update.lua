
return function(gate)
	if gate.ports[1]:isrising() then
		gate.tickStarted = gate:gettick()
	elseif gate.ports[1]:isfalling() then
		local ticksOn = gate:gettick() - gate.tickStarted
		
		local colorid = ((ticksOn-1) % 8)
		local colorname = string.reverse(tobitstring(colorid, 3))
		
		gate:cb(colorname)
	end
end
