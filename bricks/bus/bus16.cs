
datablock fxDTSBrickData(LogicGate_16BitBuffer_Data){
	brickFile = $LuaLogic::Path @ "/bricks/blb/16bitbus.blb";
	category = "Logic Bricks";
	subCategory = "Bus";
	uiName = "16 Bit Buffer";
	iconName = $LuaLogic::Path @ "icons/16 Bit Buffer";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 0;
	
	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;
	
	logicUIName = "16 Bit Buffer";
	logicUIDesc = "16 bit buffer with clock propagate";
	
	logicUpdate =
		"return function(gate) " @
		"	if gate.ports[33].state then " @
		"		for i = 1, 16 do " @
		"			gate.ports[i+16]:setstate(gate.ports[i].state) " @
		"		end " @
		"		gate.ports[34]:setstate(true) " @
		"	else " @
		"		for i = 1, 16 do " @
		"			gate.ports[i+16]:setstate(false) " @
		"		end " @
		"		gate.ports[34]:setstate(false) " @
		"	end " @
		"end "
	;
	
	numLogicPorts = 34;
	
	logicPortType[0] = 1;
	logicPortPos[0] = "0 -15 0";
	logicPortDir[0] = 0;
	logicPortUIName[0] = "In0";
	
	logicPortType[1] = 1;
	logicPortPos[1] = "0 -13 0";
	logicPortDir[1] = 0;
	logicPortUIName[1] = "In1";
	
	logicPortType[2] = 1;
	logicPortPos[2] = "0 -11 0";
	logicPortDir[2] = 0;
	logicPortUIName[2] = "In2";
	
	logicPortType[3] = 1;
	logicPortPos[3] = "0 -9 0";
	logicPortDir[3] = 0;
	logicPortUIName[3] = "In3";
	
	logicPortType[4] = 1;
	logicPortPos[4] = "0 -7 0";
	logicPortDir[4] = 0;
	logicPortUIName[4] = "In4";
	
	logicPortType[5] = 1;
	logicPortPos[5] = "0 -5 0";
	logicPortDir[5] = 0;
	logicPortUIName[5] = "In5";
	
	logicPortType[6] = 1;
	logicPortPos[6] = "0 -3 0";
	logicPortDir[6] = 0;
	logicPortUIName[6] = "In6";
	
	logicPortType[7] = 1;
	logicPortPos[7] = "0 -1 0";
	logicPortDir[7] = 0;
	logicPortUIName[7] = "In7";
	
	logicPortType[8] = 1;
	logicPortPos[8] = "0 1 0";
	logicPortDir[8] = 0;
	logicPortUIName[8] = "In8";
	
	logicPortType[9] = 1;
	logicPortPos[9] = "0 3 0";
	logicPortDir[9] = 0;
	logicPortUIName[9] = "In9";
	
	logicPortType[10] = 1;
	logicPortPos[10] = "0 5 0";
	logicPortDir[10] = 0;
	logicPortUIName[10] = "In10";
	
	logicPortType[11] = 1;
	logicPortPos[11] = "0 7 0";
	logicPortDir[11] = 0;
	logicPortUIName[11] = "In11";
	
	logicPortType[12] = 1;
	logicPortPos[12] = "0 9 0";
	logicPortDir[12] = 0;
	logicPortUIName[12] = "In12";
	
	logicPortType[13] = 1;
	logicPortPos[13] = "0 11 0";
	logicPortDir[13] = 0;
	logicPortUIName[13] = "In13";
	
	logicPortType[14] = 1;
	logicPortPos[14] = "0 13 0";
	logicPortDir[14] = 0;
	logicPortUIName[14] = "In14";
	
	logicPortType[15] = 1;
	logicPortPos[15] = "0 15 0";
	logicPortDir[15] = 0;
	logicPortUIName[15] = "In15";
	
	logicPortType[16] = 0;
	logicPortPos[16] = "0 -15 0";
	logicPortDir[16] = 2;
	logicPortUIName[16] = "Out0";
	
	logicPortType[17] = 0;
	logicPortPos[17] = "0 -13 0";
	logicPortDir[17] = 2;
	logicPortUIName[17] = "Out1";
	
	logicPortType[18] = 0;
	logicPortPos[18] = "0 -11 0";
	logicPortDir[18] = 2;
	logicPortUIName[18] = "Out2";
	
	logicPortType[19] = 0;
	logicPortPos[19] = "0 -9 0";
	logicPortDir[19] = 2;
	logicPortUIName[19] = "Out3";
	
	logicPortType[20] = 0;
	logicPortPos[20] = "0 -7 0";
	logicPortDir[20] = 2;
	logicPortUIName[20] = "Out4";
	
	logicPortType[21] = 0;
	logicPortPos[21] = "0 -5 0";
	logicPortDir[21] = 2;
	logicPortUIName[21] = "Out5";
	
	logicPortType[22] = 0;
	logicPortPos[22] = "0 -3 0";
	logicPortDir[22] = 2;
	logicPortUIName[22] = "Out6";
	
	logicPortType[23] = 0;
	logicPortPos[23] = "0 -1 0";
	logicPortDir[23] = 2;
	logicPortUIName[23] = "Out7";
	
	logicPortType[24] = 0;
	logicPortPos[24] = "0 1 0";
	logicPortDir[24] = 2;
	logicPortUIName[24] = "Out8";
	
	logicPortType[25] = 0;
	logicPortPos[25] = "0 3 0";
	logicPortDir[25] = 2;
	logicPortUIName[25] = "Out9";
	
	logicPortType[26] = 0;
	logicPortPos[26] = "0 5 0";
	logicPortDir[26] = 2;
	logicPortUIName[26] = "Out10";
	
	logicPortType[27] = 0;
	logicPortPos[27] = "0 7 0";
	logicPortDir[27] = 2;
	logicPortUIName[27] = "Out11";
	
	logicPortType[28] = 0;
	logicPortPos[28] = "0 9 0";
	logicPortDir[28] = 2;
	logicPortUIName[28] = "Out12";
	
	logicPortType[29] = 0;
	logicPortPos[29] = "0 11 0";
	logicPortDir[29] = 2;
	logicPortUIName[29] = "Out13";
	
	logicPortType[30] = 0;
	logicPortPos[30] = "0 13 0";
	logicPortDir[30] = 2;
	logicPortUIName[30] = "Out14";
	
	logicPortType[31] = 0;
	logicPortPos[31] = "0 15 0";
	logicPortDir[31] = 2;
	logicPortUIName[31] = "Out15";
	
	logicPortType[32] = 1;
	logicPortPos[32] = "0 -15 0";
	logicPortDir[32] = 3;
	logicPortUIName[32] = "ClockIn";
	logicPortCauseUpdate[32] = true;
	
	logicPortType[33] = 0;
	logicPortPos[33] = "0 15 0";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "ClockOut";
};

datablock fxDtsBrickData(LogicGate_16BitBufferAL_Data : LogicGate_16BitBuffer_Data){
	uiName = "16 Bit Buffer Active Low";
	iconName = $LuaLogic::Path @ "icons/16 Bit Buffer Active Low";
	
	logicUIName = "16 Bit Buffer Active Low";
	logicUIDesc = "16 big buffer with clock propagate; clock is active low";
	
	logicUpdate =
		"return function(gate) " @
		"	if not gate.ports[33].state then " @
		"		for i = 1, 16 do " @
		"			gate.ports[i+16]:setstate(gate.ports[i].state) " @
		"		end " @
		"		gate.ports[34]:setstate(false) " @
		"	else " @
		"		for i = 1, 16 do " @
		"			gate.ports[i+16]:setstate(false) " @
		"		end " @
		"		gate.ports[34]:setstate(true) " @
		"	end " @
		"end "
	;
};

datablock fxDtsBrickData(LogicGate_16BitDFlipFlop_Data : LogicGate_16BitBuffer_Data){
	uiName = "16 Bit D FlipFlop";
	iconName = $LuaLogic::Path @ "icons/16 Bit D FlipFlop";
	
	logicUIName = "16 Bit D FlipFlop";
	logicUIDesc = "16 big D FlipFlop with clock propagate";
	
	logicUpdate =
		"return function(gate) " @
		"	if gate.ports[33]:isrising() then " @
		"		for i = 1, 16 do " @
		"			gate.ports[i+16]:setstate(gate.ports[i].state) " @
		"		end " @
		"	end " @
		"	gate.ports[34]:setstate(gate.ports[33].state) " @
		"end "
	;
};

datablock fxDtsBrickData(LogicGate_16BitDFlipFlopAL_Data : LogicGate_16BitBuffer_Data){
	uiName = "16 Bit D FlipFlop Active Low";
	iconName = $LuaLogic::Path @ "icons/16 Bit D FlipFlop Active Low";
	
	logicUIName = "16 Bit D FlipFlop Active Low";
	logicUIDesc = "16 big D FlipFlop with clock propagate; clock is active low";
	
	logicUpdate =
		"return function(gate) " @
		"	if gate.ports[33]:isfalling() then " @
		"		for i = 1, 16 do " @
		"			gate.ports[i+16]:setstate(gate.ports[i].state) " @
		"		end " @
		"	end " @
		"	gate.ports[34]:setstate(gate.ports[33].state) " @
		"end "
	;
};
