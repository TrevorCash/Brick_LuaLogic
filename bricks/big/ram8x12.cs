datablock fxDTSBrickData(LogicGate_ram8x12_Data)
{
	brickFile = "config/server/IllogicGateMaker/ram8x12.blb";
	category = "Logic Bricks";
	subCategory = "Gatemaker";
	uiName = "ram8x12";
	iconName = "";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "ram8x12";
	logicUIDesc = "";

	numLogicPorts = 31;

	logicPortType[0] = 1;
	logicPortPos[0] = "7 -11 -3";
	logicPortDir[0] = 3;
	logicPortUIName[0] = "";

	logicPortType[1] = 1;
	logicPortPos[1] = "5 -11 -3";
	logicPortDir[1] = 3;
	logicPortUIName[1] = "";

	logicPortType[2] = 1;
	logicPortPos[2] = "3 -11 -3";
	logicPortDir[2] = 3;
	logicPortUIName[2] = "";

	logicPortType[3] = 1;
	logicPortPos[3] = "1 -11 -3";
	logicPortDir[3] = 3;
	logicPortUIName[3] = "";

	logicPortType[4] = 1;
	logicPortPos[4] = "-1 -11 -3";
	logicPortDir[4] = 3;
	logicPortUIName[4] = "";

	logicPortType[5] = 1;
	logicPortPos[5] = "-3 -11 -3";
	logicPortDir[5] = 3;
	logicPortUIName[5] = "";

	logicPortType[6] = 1;
	logicPortPos[6] = "-5 -11 -3";
	logicPortDir[6] = 3;
	logicPortUIName[6] = "";

	logicPortType[7] = 1;
	logicPortPos[7] = "-7 -11 -3";
	logicPortDir[7] = 3;
	logicPortUIName[7] = "";

	logicPortType[8] = 0;
	logicPortPos[8] = "7 -11 -2";
	logicPortDir[8] = 3;
	logicPortUIName[8] = "";

	logicPortType[9] = 0;
	logicPortPos[9] = "5 -11 -2";
	logicPortDir[9] = 3;
	logicPortUIName[9] = "";

	logicPortType[10] = 0;
	logicPortPos[10] = "3 -11 -2";
	logicPortDir[10] = 3;
	logicPortUIName[10] = "";

	logicPortType[11] = 0;
	logicPortPos[11] = "1 -11 -2";
	logicPortDir[11] = 3;
	logicPortUIName[11] = "";

	logicPortType[12] = 0;
	logicPortPos[12] = "-1 -11 -2";
	logicPortDir[12] = 3;
	logicPortUIName[12] = "";

	logicPortType[13] = 0;
	logicPortPos[13] = "-3 -11 -2";
	logicPortDir[13] = 3;
	logicPortUIName[13] = "";

	logicPortType[14] = 0;
	logicPortPos[14] = "-5 -11 -2";
	logicPortDir[14] = 3;
	logicPortUIName[14] = "";

	logicPortType[15] = 0;
	logicPortPos[15] = "-7 -11 -2";
	logicPortDir[15] = 3;
	logicPortUIName[15] = "";

	logicPortType[16] = 1;
	logicPortPos[16] = "7 11 -3";
	logicPortDir[16] = 2;
	logicPortUIName[16] = "";

	logicPortType[17] = 1;
	logicPortPos[17] = "7 9 -3";
	logicPortDir[17] = 2;
	logicPortUIName[17] = "";

	logicPortType[18] = 1;
	logicPortPos[18] = "7 7 -3";
	logicPortDir[18] = 2;
	logicPortUIName[18] = "";

	logicPortType[19] = 1;
	logicPortPos[19] = "7 5 -3";
	logicPortDir[19] = 2;
	logicPortUIName[19] = "";

	logicPortType[20] = 1;
	logicPortPos[20] = "7 3 -3";
	logicPortDir[20] = 2;
	logicPortUIName[20] = "";

	logicPortType[21] = 1;
	logicPortPos[21] = "7 1 -3";
	logicPortDir[21] = 2;
	logicPortUIName[21] = "";

	logicPortType[22] = 1;
	logicPortPos[22] = "7 -1 -3";
	logicPortDir[22] = 2;
	logicPortUIName[22] = "";

	logicPortType[23] = 1;
	logicPortPos[23] = "7 -3 -3";
	logicPortDir[23] = 2;
	logicPortUIName[23] = "";

	logicPortType[24] = 1;
	logicPortPos[24] = "7 -5 -3";
	logicPortDir[24] = 2;
	logicPortUIName[24] = "";

	logicPortType[25] = 1;
	logicPortPos[25] = "7 -7 -3";
	logicPortDir[25] = 2;
	logicPortUIName[25] = "";

	logicPortType[26] = 1;
	logicPortPos[26] = "7 -9 -3";
	logicPortDir[26] = 2;
	logicPortUIName[26] = "";

	logicPortType[27] = 1;
	logicPortPos[27] = "7 -11 -3";
	logicPortDir[27] = 2;
	logicPortUIName[27] = "";

	logicPortType[28] = 1;
	logicPortPos[28] = "-7 -9 -3";
	logicPortDir[28] = 0;
	logicPortUIName[28] = "";

	logicPortType[29] = 1;
	logicPortPos[29] = "-7 -7 -3";
	logicPortDir[29] = 0;
	logicPortUIName[29] = "";

	logicPortType[30] = 1;
	logicPortPos[30] = "-7 -5 -3";
	logicPortDir[30] = 0;
	logicPortUIName[30] = "";
};

function LogicGate_ram8x12_Data::doLogic(%this, %obj)
{
	
}

function LogicGate_ram8x12_Data::Logic_onGateAdded(%this, %obj)
{
	
}

function LogicGate_ram8x12_Data::Logic_onInput(%this, %obj, %pos, %norm)
{
	
}
