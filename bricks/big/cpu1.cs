datablock fxDTSBrickData(LogicGate_cpu1_Data)
{
	brickFile = "config/server/IllogicGateMaker/cpu1.blb";
	category = "Logic Bricks";
	subCategory = "Gatemaker";
	uiName = "cpu1";
	iconName = "";
	hasPrint = 1;
	printAspectRatio = "Logic";
	orientationFix = 3;

	isLogic = true;
	isLogicGate = true;
	isLogicInput = false;

	logicUIName = "cpu1";
	logicUIDesc = "";

	numLogicPorts = 37;

	logicPortType[0] = 1;
	logicPortPos[0] = "27 7 -3";
	logicPortDir[0] = 1;
	logicPortUIName[0] = "";

	logicPortType[1] = 1;
	logicPortPos[1] = "25 7 -3";
	logicPortDir[1] = 1;
	logicPortUIName[1] = "";

	logicPortType[2] = 1;
	logicPortPos[2] = "23 7 -3";
	logicPortDir[2] = 1;
	logicPortUIName[2] = "";

	logicPortType[3] = 1;
	logicPortPos[3] = "21 7 -3";
	logicPortDir[3] = 1;
	logicPortUIName[3] = "";

	logicPortType[4] = 1;
	logicPortPos[4] = "19 7 -3";
	logicPortDir[4] = 1;
	logicPortUIName[4] = "";

	logicPortType[5] = 1;
	logicPortPos[5] = "17 7 -3";
	logicPortDir[5] = 1;
	logicPortUIName[5] = "";

	logicPortType[6] = 1;
	logicPortPos[6] = "15 7 -3";
	logicPortDir[6] = 1;
	logicPortUIName[6] = "";

	logicPortType[7] = 1;
	logicPortPos[7] = "13 7 -3";
	logicPortDir[7] = 1;
	logicPortUIName[7] = "";

	logicPortType[8] = 0;
	logicPortPos[8] = "27 7 -2";
	logicPortDir[8] = 1;
	logicPortUIName[8] = "";

	logicPortType[9] = 0;
	logicPortPos[9] = "25 7 -2";
	logicPortDir[9] = 1;
	logicPortUIName[9] = "";

	logicPortType[10] = 0;
	logicPortPos[10] = "23 7 -2";
	logicPortDir[10] = 1;
	logicPortUIName[10] = "";

	logicPortType[11] = 0;
	logicPortPos[11] = "21 7 -2";
	logicPortDir[11] = 1;
	logicPortUIName[11] = "";

	logicPortType[12] = 0;
	logicPortPos[12] = "19 7 -2";
	logicPortDir[12] = 1;
	logicPortUIName[12] = "";

	logicPortType[13] = 0;
	logicPortPos[13] = "17 7 -2";
	logicPortDir[13] = 1;
	logicPortUIName[13] = "";

	logicPortType[14] = 0;
	logicPortPos[14] = "15 7 -2";
	logicPortDir[14] = 1;
	logicPortUIName[14] = "";

	logicPortType[15] = 0;
	logicPortPos[15] = "13 7 -2";
	logicPortDir[15] = 1;
	logicPortUIName[15] = "";

	logicPortType[16] = 0;
	logicPortPos[16] = "7 7 -3";
	logicPortDir[16] = 1;
	logicPortUIName[16] = "";

	logicPortType[17] = 0;
	logicPortPos[17] = "5 7 -3";
	logicPortDir[17] = 1;
	logicPortUIName[17] = "";

	logicPortType[18] = 0;
	logicPortPos[18] = "3 7 -3";
	logicPortDir[18] = 1;
	logicPortUIName[18] = "";

	logicPortType[19] = 0;
	logicPortPos[19] = "1 7 -3";
	logicPortDir[19] = 1;
	logicPortUIName[19] = "";

	logicPortType[20] = 0;
	logicPortPos[20] = "-1 7 -3";
	logicPortDir[20] = 1;
	logicPortUIName[20] = "";

	logicPortType[21] = 0;
	logicPortPos[21] = "-3 7 -3";
	logicPortDir[21] = 1;
	logicPortUIName[21] = "";

	logicPortType[22] = 0;
	logicPortPos[22] = "-5 7 -3";
	logicPortDir[22] = 1;
	logicPortUIName[22] = "";

	logicPortType[23] = 0;
	logicPortPos[23] = "-7 7 -3";
	logicPortDir[23] = 1;
	logicPortUIName[23] = "";

	logicPortType[24] = 0;
	logicPortPos[24] = "-9 7 -3";
	logicPortDir[24] = 1;
	logicPortUIName[24] = "";

	logicPortType[25] = 0;
	logicPortPos[25] = "-11 7 -3";
	logicPortDir[25] = 1;
	logicPortUIName[25] = "";

	logicPortType[26] = 0;
	logicPortPos[26] = "-13 7 -3";
	logicPortDir[26] = 1;
	logicPortUIName[26] = "";

	logicPortType[27] = 0;
	logicPortPos[27] = "-15 7 -3";
	logicPortDir[27] = 1;
	logicPortUIName[27] = "";

	logicPortType[28] = 0;
	logicPortPos[28] = "-17 7 -3";
	logicPortDir[28] = 1;
	logicPortUIName[28] = "";

	logicPortType[29] = 0;
	logicPortPos[29] = "-19 7 -3";
	logicPortDir[29] = 1;
	logicPortUIName[29] = "";

	logicPortType[30] = 0;
	logicPortPos[30] = "-21 7 -3";
	logicPortDir[30] = 1;
	logicPortUIName[30] = "";

	logicPortType[31] = 0;
	logicPortPos[31] = "-23 7 -3";
	logicPortDir[31] = 1;
	logicPortUIName[31] = "";

	logicPortType[32] = 0;
	logicPortPos[32] = "-27 7 -3";
	logicPortDir[32] = 1;
	logicPortUIName[32] = "";

	logicPortType[33] = 0;
	logicPortPos[33] = "-29 7 -3";
	logicPortDir[33] = 1;
	logicPortUIName[33] = "";

	logicPortType[34] = 1;
	logicPortPos[34] = "27 -7 -3";
	logicPortDir[34] = 3;
	logicPortUIName[34] = "";

	logicPortType[35] = 1;
	logicPortPos[35] = "23 -7 -3";
	logicPortDir[35] = 3;
	logicPortUIName[35] = "";

	logicPortType[36] = 1;
	logicPortPos[36] = "19 -7 -3";
	logicPortDir[36] = 3;
	logicPortUIName[36] = "";
};

function LogicGate_cpu1_Data::doLogic(%this, %obj)
{
	
}

function LogicGate_cpu1_Data::Logic_onGateAdded(%this, %obj)
{
	
}

function LogicGate_cpu1_Data::Logic_onInput(%this, %obj, %pos, %norm)
{
	
}
